(define-package "telega" "20210203.1317" "Telegram client (unofficial)"
  '((emacs "26.1")
    (visual-fill-column "1.9")
    (rainbow-identifiers "0.2.2"))
  :commit "cfe4e938c1942d35b0dee4952de37af688ed6551" :authors
  '(("Zajcev Evgeny" . "zevlg@yandex.ru"))
  :maintainer
  '("Zajcev Evgeny" . "zevlg@yandex.ru")
  :keywords
  '("comm")
  :url "https://github.com/zevlg/telega.el")
;; Local Variables:
;; no-byte-compile: t
;; End:
